## Installation ##

After cloning the repository, the following commands install the Javascript dependencies:

    npm install
    bower install

## Building and starting the server

Run the following command to start a server:

    gulp

After the server starts, the application is accessible at the following URL:

    http://localhost:3000