angular
	.module('app.report')
  .factory('reportsFactory', reportsFactory)

function reportsFactory () {
  var service = {}
  service.add = add
  service.getByIndex = getByIndex
  service.getReports = getReports
  service.update = update
  service.remove = remove

  service.reports = [
    {
      title: 'Report for Board of Directors',
      updated: 1472407688002,
      status: 1,
      activeColors: {
        descriptionsColors: '#aaa',
        headlinesColors: '#00bfa5',
        textColors: '#032c3e'
      },
      chapters: [
        {
          title: 'Introduction',
          parts: [
            {
              headline: 'Foreword',
              text: 'Welcome to the report of the survey “How do you rate our Customer Service” conducted between 5th and 15th of July 2016.'
            },
            {
              headline: 'Table of Contents',
              description: 'Generated automatically'
            },
            {
              headline: 'Summary report',
              description: 'Generated automatically'
            }
          ]
        },
        {
          title: 'Chapter 1: Research study',
          parts: [
            {
              headline: 'Introduction',
              text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.'
            },
            {
              headline: '1. Overall, how friendly were the hotel staff members?',
              description: 'Give us an honest answer'
            },
            {
              headline: '2. How often do you check Facebook?',
              description: 'Give us an honest answer'
            }
          ]
        }
      ]
    },
    {
      title: 'Draft of report',
      status: 0,
      activeColors: {
        descriptionsColors: '#aaa',
        headlinesColors: '#00bfa5',
        textColors: '#032c3e'
      },
      chapters: [
        {
          title: 'Introduction',
          parts: [
            {
              headline: 'Foreword',
              text: 'Welcome to the report of the survey “How do you rate our Customer Service” conducted between 5th and 15th of July 2016.'
            }
          ]
        }
      ]
    }
  ]

  return service

  function add (report) {
    var length = service.reports.length
    service.reports[length] = report
  }

  function getByIndex (index) {
    return service.reports[index]
  }

  function getReports () {
    return service.reports
  }

  function update (index, report) {
    service.reports[index] = report
  }

  function remove (report) {
    var index = service.reports.indexOf(report)
    service.reports.splice(index, 1)
  }
}
