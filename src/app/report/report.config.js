angular
  .module('app.report')
  .config(config)

/* @ngInject */
function config ($routeProvider) {
  $routeProvider
    .when('/report', {
      templateUrl: 'app/report/list.html',
      controller: 'ReportsController',
      controllerAs: 'vm'
    })
    .when('/report/generate', {
      templateUrl: 'app/report/generate.html',
      controller: 'GenerateController',
      controllerAs: 'vm'
    })
    .when('/report/generate/:reportIndex', {
      templateUrl: 'app/report/generate.html',
      controller: 'GenerateController',
      controllerAs: 'vm'
    })
    .otherwise({
      redirectTo: '/report'
    })
}
