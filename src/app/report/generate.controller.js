angular
  .module('app.report')
  .controller('GenerateController', GenerateController)

/* @ngInject */
function GenerateController ($location, $routeParams, $window, reportsFactory) {
  var vm = this
  vm.addChapter = addChapter
  vm.addPart = addPart
  vm.colors = {
    descriptionsColors: [
      {
        hex: '#00bfa5',
        lightText: true
      },
      {
        hex: '#032c3e',
        lightText: true
      },
      {
        hex: '#aaa',
        lightText: true
      },
      {
        hex: '#000',
        lightText: true
      },
      {
        hex: '#222',
        lightText: true
      },
      {
        hex: '#444',
        lightText: true
      },
      {
        hex: '#666',
        lightText: true
      },
      {
        hex: '#bbb',
        lightText: true
      },
      {
        hex: '#ddd'
      }
    ],
    headlinesColors: [
      {
        hex: '#00bfa5',
        lightText: true
      },
      {
        hex: '#032c3e',
        lightText: true
      },
      {
        hex: '#aaa',
        lightText: true
      },
      {
        hex: '#000',
        lightText: true
      },
      {
        hex: '#222',
        lightText: true
      },
      {
        hex: '#444',
        lightText: true
      },
      {
        hex: '#666',
        lightText: true
      },
      {
        hex: '#bbb',
        lightText: true
      },
      {
        hex: '#ddd'
      }
    ],
    textColors: [
      {
        hex: '#00bfa5',
        lightText: true
      },
      {
        hex: '#032c3e',
        lightText: true
      },
      {
        hex: '#aaa',
        lightText: true
      },
      {
        hex: '#000',
        lightText: true
      },
      {
        hex: '#222',
        lightText: true
      },
      {
        hex: '#444',
        lightText: true
      },
      {
        hex: '#666',
        lightText: true
      },
      {
        hex: '#bbb',
        lightText: true
      },
      {
        hex: '#ddd'
      }
    ]
  }
  vm.defaultReport = {
    title: '',
    status: 0,
    activeColors: {
      descriptionsColors: '#aaa',
      headlinesColors: '#00bfa5',
      textColors: '#032c3e'
    },
    chapters: []
  }
  vm.generate = generate
  vm.index
  vm.removeChapter = removeChapter
  vm.removePart = removePart
  vm.report = {}
  vm.save = save

  activate()

  function activate () {
    if ($routeParams.reportIndex) {
      vm.report = reportsFactory.reports[$routeParams.reportIndex]
    } else {
      vm.report = vm.defaultReport
    }

    setActiveColors()
  }

  function addChapter () {
    var length = vm.report.chapters.length
    vm.report.chapters[length] = {
      title: 'New chapter',
      parts: [
        {
          headline: 'New part\'s headline',
          text: 'New part\'s text',
          description: 'New part\'s description'
        }
      ]
    }
  }

  function addPart (chapter) {
    var length = chapter.parts.length
    chapter.parts[length] = {
      headline: 'New part\'s headline',
      text: 'New part\'s text',
      description: 'New part\'s description'
    }
  }

  function generate (report) {
    if (!vm.report.title.length) {
      $window.alert('Please pass the title of report before generating.')
      return
    }

    saveActiveColors()
    vm.report.status = 1
    vm.report.updated = new Date()

    if (typeof $routeParams.reportIndex !== 'undefined') {
      console.log('tarara')
      var reportToRemove = reportsFactory.getByIndex($routeParams.reportIndex)
      reportsFactory.remove(reportToRemove)
    }

    reportsFactory.add(vm.report)

    $location.path('/#/report')
  }

  function removeChapter (chapter) {
    var index = vm.report.chapters.indexOf(chapter)
    vm.report.chapters.splice(index, 1)
  }

  function removePart (chapter, part) {
    var index = chapter.parts.indexOf(part)
    chapter.parts.splice(index, 1)
  }

  function save (report) {
    if (!vm.report.title.length) {
      $window.alert('Please pass the title of report before saving.')
      return
    }

    saveActiveColors()
    vm.report.updated = new Date()

    if (typeof $routeParams.reportIndex !== 'undefined') {
      reportsFactory.update($routeParams.reportIndex, vm.report)
    } else {
      reportsFactory.add(vm.report)
    }

    $location.path('/#/report')
  }

  function saveActiveColors () {
    angular.forEach(vm.colors, function (colorSet, key) {
      var newColor = colorSet.filter(function (color) {
        if (color.active) {
          return true
        }
      })[0]

      vm.report.activeColors[key] = newColor.hex
    })
  }

  function setActiveColors () {
    angular.forEach(vm.report.activeColors, function (hex, key) {
      var colorSet = vm.colors[key]

      var color = colorSet.filter(function (color) {
        if (color.hex === hex) {
          return true
        }
      })[0]

      color.active = true
    })
  }
}
