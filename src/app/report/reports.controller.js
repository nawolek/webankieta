angular
  .module('app.report')
  .controller('ReportsController', ReportsController)

/* @ngInject */
function ReportsController (reportsFactory) {
  var vm = this
  vm.draftsFilter = draftsFilter
  vm.remove = remove
  vm.reports = reportsFactory.getReports()
  vm.reportsFilter = reportsFilter

  function draftsFilter (item) {
    return item.status === 0
  }

  function remove (report) {
    reportsFactory.remove(report)
  }

  function reportsFilter (item) {
    return item.status > 0
  }
}
