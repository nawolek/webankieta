;(function () {
  angular
    .module('app', [
      // Angular
      'ngRoute',

      // Shared
      'app.widgets',

      // Feature areas
      'app.report'
    ])
})()
