angular
  .module('app.widgets')
  .directive('colors', colors)

function colors ($filter, $timeout) {
  return {
    restrict: 'E',
    templateUrl: 'app/widgets/colors.html',
    scope: {
      list: '<',
      title: '@',
      selector: '@'
    },
    link: function (scope) {
      scope.activeColor
      scope.itemsToColor = {}
      scope.updateColor = updateColor

      activate()

      function activate () {
        scope.activeColor = getActiveColor()

        $timeout(function () {
          scope.itemsToColor = document.querySelectorAll(scope.selector)
          paintItems(scope.activeColor)
        })
      }

      function getActiveColor () {
        var color = scope.list.filter(function (color) {
          return color.active
        })[0]

        return color
      }

      function updateColor (newColor) {
        scope.itemsToColor = document.querySelectorAll(scope.selector)

        angular.forEach(scope.list, function (color) {
          if (newColor !== color) {
            if (color.active) color.active = false
          } else {
            color.active = true
          }
        })

        paintItems(newColor)

        scope.activeColor = newColor
      }

      function paintItems (newColor) {
        angular.forEach(scope.itemsToColor, function (item) {
          item.style.color = newColor.hex
        })
      }
    }
  }
}
