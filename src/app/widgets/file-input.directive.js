angular
  .module('app')
  .directive('fileInput', fileInput)

function fileInput () {
  return {
    restrict: 'E',
    templateUrl: 'app/widgets/file-input.html',
    scope: {
      inputId: '@'
    },
    link: function (scope, element) {
      var input = element.find('input')

      input.on('change', function (event) {
        var files = this.files

        if (files.length) {
          var fileName = files[0].name
          scope.$apply(function () {
            scope.fileName = fileName
          })
        }
      })
    }
  }
}
